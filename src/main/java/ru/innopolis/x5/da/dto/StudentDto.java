package main.java.ru.innopolis.x5.da.dto;

import java.util.Date;

public class StudentDto {
    private Integer id;
    private String firstName;
    private String middleName;
    private String lastName;
    private Date dateOfBirth;
    private Integer studyGroup;
    private String city;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Integer getStudyGroup() {
        return studyGroup;
    }

    public void setStudyGroup(Integer studyGroup) {
        this.studyGroup = studyGroup;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "StudentDto{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", studyGroup='" + studyGroup + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
