package main.java.ru.innopolis.x5.da.dto;

import java.util.Date;

public class StudyGroupDto {
    private Integer id;
    private String name;
    private Date graduation;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getGraduation() {
        return graduation;
    }

    public void setGraduation(Date graduation) {
        this.graduation = graduation;
    }

    @Override
    public String toString() {
        return "StudyGroupDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", graduation=" + graduation +
                '}';
    }
}