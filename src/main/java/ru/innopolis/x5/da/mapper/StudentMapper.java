package main.java.ru.innopolis.x5.da.mapper;

import main.java.ru.innopolis.x5.da.dto.StudentDto;

import java.sql.ResultSet;
import java.sql.SQLException;

public final class StudentMapper {
    private StudentMapper() {
    }

    public static StudentDto mapResultSetToDto(ResultSet resultSet) throws SQLException {
        StudentDto studentDto = new StudentDto();
        if (resultSet.next()) {
            studentDto.setId(resultSet.getInt(1));
            studentDto.setFirstName(resultSet.getString(2));
            studentDto.setMiddleName(resultSet.getString(3));
            studentDto.setLastName(resultSet.getString(4));
            studentDto.setDateOfBirth(resultSet.getDate(5));
            studentDto.setStudyGroup(resultSet.getInt(6));
            studentDto.setCity(resultSet.getString(7));
        }
        return studentDto;
    }
}
