package main.java.ru.innopolis.x5.da.mapper;

import main.java.ru.innopolis.x5.da.dto.CityDto;

import java.sql.ResultSet;
import java.sql.SQLException;

public final class CityMapper {
    private CityMapper() {}

    public static CityDto mapResultSetToDto(ResultSet resultSet) throws SQLException {
        CityDto cityDto = new CityDto();
        if (resultSet.next()) {
            cityDto.setId(resultSet.getInt(1));
            cityDto.setName(resultSet.getString(2));
        }
        return cityDto;
    }
}
