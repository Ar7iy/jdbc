package main.java.ru.innopolis.x5.da.mapper;

import main.java.ru.innopolis.x5.da.dto.StudyGroupDto;

import java.sql.ResultSet;
import java.sql.SQLException;

public final class StudyGroupMapper {
    private StudyGroupMapper() {}

    public static StudyGroupDto mapResultSetToDto(ResultSet resultSet) throws SQLException {
        StudyGroupDto groupDto = new StudyGroupDto();
        if (resultSet.next()) {
            groupDto.setId(resultSet.getInt(1));
            groupDto.setName(resultSet.getString(2));
            groupDto.setGraduation(resultSet.getDate(3));
        }
        return groupDto;
    }
}
