package main.java.ru.innopolis.x5.da;

import main.java.ru.innopolis.x5.da.dto.CityDto;

public interface CityDao {
    CityDto getCityById(Integer id);

    CityDto updateById(Integer id, CityDto newValues);

    void createCity(CityDto newCity);

    void deleteCity(Integer id);
}
