package main.java.ru.innopolis.x5.da;

import main.java.ru.innopolis.x5.da.dto.StudentDto;

public interface StudentDao {
    StudentDto getStudentById(Integer id);

    StudentDto updateById(Integer id, StudentDto newValues);

    void createStudent(StudentDto newStudent);

    void deleteStudent(Integer id);
}
