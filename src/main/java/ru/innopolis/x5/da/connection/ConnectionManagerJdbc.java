package main.java.ru.innopolis.x5.da.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class ConnectionManagerJdbc {
    private final Logger logger = LogManager.getRootLogger();
    private static final String HARDPASS = "pass";

    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/postgres",
                    "postgres", HARDPASS);
        } catch (SQLException e) {
            logger.error("unable to create connection at " + this.getClass().getName() + " method - getConnection");
        }
        return connection;
    }
}
