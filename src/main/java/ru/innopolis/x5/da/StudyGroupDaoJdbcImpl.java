package main.java.ru.innopolis.x5.da;

import main.java.ru.innopolis.x5.da.connection.ConnectionManagerJdbc;
import main.java.ru.innopolis.x5.da.dto.StudyGroupDto;
import main.java.ru.innopolis.x5.da.mapper.StudyGroupMapper;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StudyGroupDaoJdbcImpl implements StudyGroupDao {
    private final Logger logger = LogManager.getLogger(this.getClass());

    private ConnectionManagerJdbc connectionManager;

    public StudyGroupDaoJdbcImpl(ConnectionManagerJdbc connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public StudyGroupDto getStudyGroupById(Integer id) {
        StudyGroupDto result = new StudyGroupDto();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM study_group WHERE id = ?");
             ResultSet resultSet = preparedStatement.executeQuery()) {
            result = StudyGroupMapper.mapResultSetToDto(resultSet);
        } catch (SQLException e) {
            logger.error(this.getClass().getName() + ": getStudyGroupById", e);
        }
        return result;
    }

    @Override
    public StudyGroupDto updateById(Integer id) {
        return null;
    }

    @Override
    public void createStudyGroup(StudyGroupDto newGroup) {
        throw new UnsupportedOperationException();
        // to do
    }

    @Override
    public void deleteStudyGroup(Integer id) {
        throw new UnsupportedOperationException();
        // to do
    }
}
