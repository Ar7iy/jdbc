package main.java.ru.innopolis.x5.da;

import main.java.ru.innopolis.x5.da.dto.StudyGroupDto;

public interface StudyGroupDao {
    StudyGroupDto getStudyGroupById(Integer id);

    StudyGroupDto updateById(Integer id);

    void createStudyGroup(StudyGroupDto newGroup);

    void deleteStudyGroup(Integer id);
}
