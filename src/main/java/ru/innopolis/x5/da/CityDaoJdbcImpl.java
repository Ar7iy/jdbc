package main.java.ru.innopolis.x5.da;

import main.java.ru.innopolis.x5.da.connection.ConnectionManagerJdbc;
import main.java.ru.innopolis.x5.da.dto.CityDto;
import main.java.ru.innopolis.x5.da.mapper.CityMapper;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CityDaoJdbcImpl implements CityDao {
    private final Logger logger = LogManager.getLogger(this.getClass());

    private ConnectionManagerJdbc connectionManager;

    public CityDaoJdbcImpl(ConnectionManagerJdbc connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public CityDto getCityById(Integer id) {
        CityDto result = new CityDto();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM city WHERE id = ?");
             ResultSet resultSet = preparedStatement.executeQuery()) {
            result = CityMapper.mapResultSetToDto(resultSet);
        } catch (SQLException e) {
            logger.error(this.getClass().getName() + " method: getCityById", e);
        }
        return result;
    }

    @Override
    public CityDto updateById(Integer id, CityDto newValues) {
        return null;
    }

    @Override
    public void createCity(CityDto newCity) {
        // to do
    }

    @Override
    public void deleteCity(Integer id) {
        // to do
    }
}
