package main.java.ru.innopolis.x5.da;

import main.java.ru.innopolis.x5.da.connection.ConnectionManagerJdbc;
import main.java.ru.innopolis.x5.da.dto.StudentDto;
import main.java.ru.innopolis.x5.da.mapper.StudentMapper;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StudentDaoJdbcImpl implements StudentDao {
    private final Logger logger = LogManager.getLogger(this.getClass());
    private ConnectionManagerJdbc connectionManager;

    public StudentDaoJdbcImpl(ConnectionManagerJdbc connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public StudentDto getStudentById(Integer id) {
        StudentDto result = new StudentDto();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM student WHERE id = ?");
             ResultSet resultSet = preparedStatement.executeQuery()) {
            result = StudentMapper.mapResultSetToDto(resultSet);
        } catch (SQLException e) {
            logger.error("Error in class " + this.getClass().getName() + " method getStudentById. ", e);
        }
        return result;
    }

    @Override
    public StudentDto updateById(Integer id, StudentDto newValues) {
        return null;
    }

    @Override
    public void createStudent(StudentDto newStudent) {
        // to do
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteStudent(Integer id) {
        // to do
        throw new UnsupportedOperationException();
    }
}
