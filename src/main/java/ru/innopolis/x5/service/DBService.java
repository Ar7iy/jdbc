package main.java.ru.innopolis.x5.service;

import main.java.ru.innopolis.x5.da.StudyGroupDao;
import main.java.ru.innopolis.x5.da.dto.StudyGroupDto;
import main.java.ru.innopolis.x5.model.StudyGroup;
import main.java.ru.innopolis.x5.model.Student;
import main.java.ru.innopolis.x5.da.StudentDao;
import main.java.ru.innopolis.x5.da.dto.StudentDto;

public class DBService {
    private StudentDao studentDao;
    private StudyGroupDao studyGroupDao;

    public DBService(StudentDao studentDao) {
        this.studentDao = studentDao;
    }

    public Student getStudent(Integer id) {
        StudentDto studentDto = studentDao.getStudentById(id);
        StudyGroupDto groupDto = studyGroupDao.getStudyGroupById(studentDto.getStudyGroup());
        StudyGroup group = new StudyGroup(groupDto.getId(), groupDto.getName(), groupDto.getGraduation());
        return new Student(studentDto.getId(),
                studentDto.getFirstName(),
                studentDto.getMiddleName(),
                studentDto.getLastName(),
                studentDto.getDateOfBirth(),
                group,
                studentDto.getCity());
    }
}
