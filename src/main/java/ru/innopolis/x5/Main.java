package main.java.ru.innopolis.x5;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);
    private static final String HARDPASS = "pass";

    public static void main(String[] args) {

        try (Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/postgres",
                "postgres", HARDPASS);
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "INSERT INTO student (first_name, middle_name) VALUES (?, ?)")) {

             preparedStatement.setString(1, "Poluekt");
             preparedStatement.setString(2, "Poluektovich");

             preparedStatement.addBatch();
             logger.info("ready");
             preparedStatement.executeBatch();
        } catch (SQLException e) {
            logger.error("Failed in Main class. ", e);
        }
    }
}
